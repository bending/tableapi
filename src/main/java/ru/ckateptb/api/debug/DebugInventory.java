package ru.ckateptb.api.debug;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.ckateptb.api.gui.TableInventory;

class DebugInventory extends TableInventory {
    DebugInventory(Player player, String name, int rows) {
        super(player, name, rows);
        ItemStack stack = DebugConfig.stack;
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName("§aP§ei§an§eg§c!");
        stack.setItemMeta(meta);
        this.setItem(0, new DebugButton(stack, this));
    }
}
