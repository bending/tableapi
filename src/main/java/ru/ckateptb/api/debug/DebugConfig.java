package ru.ckateptb.api.debug;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.api.config.Configurable;

@Configurable.ConfigFile(header = "Пример сознадия конфига используя TableAPI")
public class DebugConfig extends Configurable {

    public DebugConfig(String path) {
        super(path);
    }

    @ConfigField(name = "debug", comment = "Этот парамерт отвечает за дебаг в плагине")
    public static Boolean debug = true;

    @ConfigField(name = "button.icon", comment = "Кнопка в меню дебага")
    static ItemStack stack = new ItemStack(Material.EMERALD);

    @ConfigField(name = "test.location", comment = "Пример локации")
    static Location location = new Location(Bukkit.getWorlds().get(0), 100, 100, 100);

}
