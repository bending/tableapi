package ru.ckateptb.api.debug;

import org.bukkit.inventory.ItemStack;
import ru.ckateptb.api.gui.TableButton;
import ru.ckateptb.api.gui.TableInventory;

public class DebugButton extends TableButton {
    DebugButton(ItemStack item, TableInventory inventory) {
        super(item, inventory);
    }

    public void onClick() {
        this.inventory.player.sendMessage("§aP§eo§an§eg§c!");
    }
}
