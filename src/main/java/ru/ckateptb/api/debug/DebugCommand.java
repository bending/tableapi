package ru.ckateptb.api.debug;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.ckateptb.api.command.TableCommand;

import java.util.ArrayList;
import java.util.List;

public class DebugCommand extends TableCommand {

    public DebugCommand() {
        super("Debug");
    }

    @Override
    public boolean progress(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            DebugInventory inv = new DebugInventory(player, "§cDebug", 1);
            player.teleport(DebugConfig.location);
            inv.open();
        }
        sender.sendMessage(DebugConfig.location.toString());
        return true;
    }

    @Override
    public List<String> tab(CommandSender sender, String[] args) {
        return new ArrayList<>();
    }
}
