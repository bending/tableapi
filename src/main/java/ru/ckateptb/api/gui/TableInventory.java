package ru.ckateptb.api.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.api.object.TableUser;

import java.util.ArrayList;
import java.util.List;

public abstract class TableInventory implements Listener {
    public String name;
    public int rows;
    public Player player;
    public Inventory inventory;
    public TableUser user;
    public List<TableButton> buttonList = new ArrayList<>();

    public TableInventory(Player player, String name, int rows) {
        this.user = TableUser.get(player);
        this.player = player;
        this.name = name;
        this.rows = rows;
        if (rows > 6 || rows < 1) {
            new IllegalArgumentException("Rows must be from 1 to 6! Set to default (3)").printStackTrace();
            this.rows = 3;
        }

        this.inventory = Bukkit.createInventory(this.player, this.rows * 9, this.name);
        Bukkit.getPluginManager().registerEvents(this, TableAPI.getPlugin());

        for (int i = 0; i < this.user.getInventoryList().size(); ++i) {
            TableInventory inv = this.user.getInventoryList().get(i);
            if (inv.getClass().equals(this.getClass())) {
                inv.delete();
            }
        }
        this.user.getInventoryList().add(this);
    }

    public void setItem(int slot, TableButton button) {
        this.inventory.setItem(slot, button.getItem());
        this.buttonList.add(button);
    }

    public void open() {
        this.player.openInventory(this.inventory);
    }

    public void delete() {
        this.user.getInventoryList().remove(this);
        for (TableButton button : this.buttonList)
            button.delete();
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getInventory().equals(this.inventory)) {
            event.setCancelled(true);
        }

    }
}

