package ru.ckateptb.api.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.api.object.ItemNBT;

import java.util.ArrayList;
import java.util.List;


public abstract class TableButton implements Listener {
    public static ItemStack air = new ItemStack(Material.AIR);
    ;
    public static long id = 0;
    public static List<Long> emptyId = new ArrayList<>();
    public ItemStack item;
    public TableInventory inventory;

    public TableButton(ItemStack item, TableInventory inventory) {
        this.setItem(item);
        this.inventory = inventory;
        Bukkit.getPluginManager().registerEvents(this, TableAPI.getPlugin());
    }

    public void setItem(ItemStack item) {
        ItemNBT nbtItem = new ItemNBT(item);
        if (emptyId.isEmpty())
            nbtItem.getTag().setLong("TableButton", TableButton.id++);
        else {
            Long id = emptyId.get(0);
            emptyId.remove(id);
            nbtItem.getTag().setLong("TableButton", id);
        }

        this.item = nbtItem.getItem();
    }

    public abstract void onClick();

    public ItemStack getItem() {
        return this.item;
    }

    public void delete() {
        emptyId.add(this.getId());
    }

    private long getId() {
        ItemNBT nbtItem = new ItemNBT(this.item);
        return nbtItem.getTag().getLong("TableButton");
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();
        Inventory inventory = e.getInventory();
        Player player = (Player) e.getWhoClicked();
        if (player != null && inventory != null && item != null) {
            if (item.equals(this.item) && inventory.equals(this.inventory.inventory) && player.equals(this.inventory.player)) {
                this.onClick();
            }
        }
    }
}
