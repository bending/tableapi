package ru.ckateptb.api.object;

import lombok.Getter;
import org.bukkit.entity.Player;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.api.gui.TableInventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class TableUser {

    public static Map<String, TableUser> users = new HashMap<>();

    public static TableUser get(Player player) {
        String uuid = player.getUniqueId().toString();
        if (users.containsKey(uuid))
            return users.get(uuid);
        return new TableUser(player);
    }

    private String uuid;
    private Player player;
    private List<TableInventory> inventoryList = new ArrayList<>();

    private TableUser(Player player) {
        this.uuid = player.getUniqueId().toString();
        this.player = player;
        users.put(this.uuid, this);
    }

    public double getBalance() {
        return TableAPI.getEconomy().getBalance(player);
    }

    public String getPrefix() {
        return TableAPI.getChat().getPlayerPrefix(player).replaceAll("&", "§");
    }

}

