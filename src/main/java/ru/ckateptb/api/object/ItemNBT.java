package ru.ckateptb.api.object;

import lombok.Getter;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

@Getter
public class ItemNBT {
    private NBTTagCompound tag;
    private net.minecraft.server.v1_12_R1.ItemStack nmsItem;

    public ItemNBT(ItemStack item) {
        this.nmsItem = CraftItemStack.asNMSCopy(item);
        if (this.nmsItem.hasTag()) {
            this.tag = this.nmsItem.getTag();
        } else {
            this.tag = new NBTTagCompound();
            this.nmsItem.setTag(this.tag);
        }
    }

    public ItemStack getItem() {
        return CraftItemStack.asBukkitCopy(this.nmsItem);
    }
}
