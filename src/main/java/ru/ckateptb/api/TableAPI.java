package ru.ckateptb.api;

import lombok.Getter;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.dependency.Dependency;
import org.bukkit.plugin.java.annotation.dependency.DependsOn;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import ru.ckateptb.api.debug.DebugCommand;
import ru.ckateptb.api.debug.DebugConfig;

import java.io.File;

@Plugin(name = TableAPI.NAME, version = TableAPI.VERSION)
@DependsOn(@Dependency("Vault"))
public class TableAPI extends JavaPlugin {

    static final String NAME = "TableAPI";
    static final String VERSION = "0.0.3";

    @Getter
    private static Economy economy = null;
    @Getter
    private static Chat chat = null;
    @Getter
    private static TableAPI plugin = null;

    public void onEnable() {
        plugin = this;
        setupVault();
        setupDebug();
    }

    private void setupVault() {
        RegisteredServiceProvider<Chat> rspChat = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rspChat.getProvider();
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        economy = rsp.getProvider();
    }

    private void setupDebug() {
        new DebugConfig(this.getDataFolder() + File.separator + "config.yml");
        if (!DebugConfig.debug)
            return;
        new DebugCommand();
    }
}
